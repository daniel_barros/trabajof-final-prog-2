from PyQt5.QtWidgets import QMainWindow, QApplication,QDialog,QInputDialog,QMessageBox
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
import sqlite3
from interfaces.Game import Juego


class Perfil(QDialog):
    def __init__(self,parent):
        super().__init__(parent=parent)
        uic.loadUi("ui/perfil.ui", self)
        self.conexion=sqlite3.connect('src/players.db')
        self.cursor=self.conexion.cursor()

        self.jugador=""
        self.game=Juego(self)
        self.modoJuego=0

        self.botonNuevo.clicked.connect(self.nuevoPerfil)
        self.botonElegir.clicked.connect(self.elegirPerfil)

    def nuevoPerfil(self):
        jugador,Ok=QInputDialog.getText(self,'Ingresar','Ingrese tu nombre o nickname')
        items = self.lista.findItems(jugador,Qt.MatchExactly)
        if len(items)>0:        
            print ("Jugador ya lista, no es posible agregar...")
            msg=QMessageBox()        
            msg.setWindowTitle('Error')
            msg.setWindowIcon(QIcon('./img/pi.png'))
            msg.setText("""
            Jugador ya se encuentra en la lista elija uno
            o agregue un nuevo perfil       
                    """)
            msg.setIcon(QMessageBox.Information)
            msg.exec_()
        else:
            print("Jugador no esta en la lista")
            self.cursor.execute(""" INSERT INTO jugador
                                (nombre,puntos)
                                VALUES (?,?)
                                 """,(jugador,0))
            self.conexion.commit()
            self.lista.addItem(jugador)        

    def elegirPerfil(self):
        if self.lista.currentItem()==None:
            self.jugador="Player"            
            self.close()
        else:
            self.jugador=str(self.lista.currentItem().text())            
            self.close()
        
        if self.radiobtnDesafio.isChecked():
            self.modoJuego=0
            print("DESAFIO!!!")
        elif self.radiobtnSumas.isChecked():
            self.modoJuego=1
            print("SUMAS")
        elif self.radiobtnRestas.isChecked():
            self.modoJuego=2
        elif self.radiobtnProductos.isChecked():
            self.modoJuego=3
        elif self.radiobtnCocientes.isChecked():
            self.modoJuego=4       

        self.parent().hide()
        self.game.show()

    def closeEvent(self, event):
        self.close()
        self.parent().show()            
    def showEvent(self,event):        
        self.lista.clear()       
        self.cursor.execute("SELECT * FROM jugador")
        gamers=self.cursor.fetchall()
        for gamer in gamers:
            self.lista.addItem(gamer[1])



