from PyQt5.QtWidgets import QMainWindow, QApplication,QDialog,QMessageBox
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
import random,sqlite3



class Juego(QMainWindow):
    def __init__(self,parent):
        super().__init__(parent=parent)
        uic.loadUi("ui/game2.ui", self)
        self.resultadoReal=0 
        self.conexion=sqlite3.connect('src/players.db')
        self.cursor=self.conexion.cursor()     
                   
        self.botonAtras.clicked.connect(self.cerrarymostrar)
        self.botonResultado1.clicked.connect(self.comprobarResultado1)
        self.botonResultado2.clicked.connect(self.comprobarResultado2)
        self.botonResultado3.clicked.connect(self.comprobarResultado3)
    
    def cerrarymostrar(self):
        msg=QMessageBox()        
        msg.setWindowTitle('Juego Terminado...')
        msg.setWindowIcon(QIcon('./img/pi.png'))
        msg.setText("Partida terminada por: "+self.parent().jugador+". Tu puntaje fue de "+self.labelPts.text()+" puntos")
        msg.setIcon(QMessageBox.Information)
        msg.exec_()

        self.cursor.execute("UPDATE jugador SET puntos=puntos+"+self.labelPts.text()+" WHERE nombre='"+self.labelPerfil.text()+"'")
        self.conexion.commit()
        self.close()
        self.parent().show()     
    def closeEvent(self, event):
        self.close()
        self.parent().show()          
    
    def comprobarResultado1(self):
        if self.resultadoReal==int(self.labelResultado1.text()):
            print("Acerto!!")
            self.juego()
            aux=int(self.labelPts.text())+1
            self.labelPts.setText(str(aux))
        else:
            print("Error!!!!!")
            aux=int(self.labelVidas.text())-1
            if aux==0:
                print("Sin vidas, Juego terminado!!")
                msg=QMessageBox()        
                msg.setWindowTitle('Juego Terminado...')
                msg.setWindowIcon(QIcon('./img/pi.png'))
                msg.setText(self.parent().jugador+" no tienes mas intentos. Tu puntaje fue de "+self.labelPts.text()+" puntos")
                msg.setIcon(QMessageBox.Information)
                msg.exec_()
                self.cerrarymostrar()
            self.labelVidas.setText(str(aux))
    def comprobarResultado2(self):
        if self.resultadoReal==int(self.labelResultado2.text()):
            print("Acerto!!")
            self.juego()
            aux=int(self.labelPts.text())+1
            self.labelPts.setText(str(aux))
        else:
            print("Error!!!!!")
            aux=int(self.labelVidas.text())-1
            if aux==0:
                print("Sin vidas, Juego terminado!!")
                msg=QMessageBox()        
                msg.setWindowTitle('Juego Terminado...')
                msg.setWindowIcon(QIcon('./img/pi.png'))
                msg.setText(self.parent().jugador+" no tienes mas intentos. Tu puntaje fue de "+self.labelPts.text()+" puntos")
                msg.setIcon(QMessageBox.Information)
                msg.exec_()
                self.cerrarymostrar()
            self.labelVidas.setText(str(aux))
    def comprobarResultado3(self):
        if self.resultadoReal==int(self.labelResultado3.text()):
            print("Acerto!!")
            self.juego()
            aux=int(self.labelPts.text())+1
            self.labelPts.setText(str(aux))
        else:
            print("Error!!!!!")
            aux=int(self.labelVidas.text())-1
            if aux==0:
                print("Sin vidas, Juego terminado!!")
                msg=QMessageBox()        
                msg.setWindowTitle('Juego Terminado...')
                msg.setWindowIcon(QIcon('./img/pi.png'))
                msg.setText(self.parent().jugador+" no tienes mas intentos. Tu puntaje fue de "+self.labelPts.text()+" puntos")
                msg.setIcon(QMessageBox.Information)
                msg.exec_()
                self.cerrarymostrar()
            self.labelVidas.setText(str(aux))

    def juego(self):
        opc=[]
        if self.parent().modoJuego==0: #Modo Desafio
            print("Holis")
            desafio='1234'
            elecc=int(random.choice(desafio))
            if elecc==1:
                a=random.randrange(50)
                b=random.randrange(50)
                c=str(a)+"+"+str(b)
                self.resultadoReal=a+b
                self.labelOperacion.setText(c)
            elif elecc==2:
                a=random.randrange(50)
                b=random.randrange(50)
                while b>a:
                    b=random.randrange(50)
                c=str(a)+"-"+str(b)
                self.resultadoReal=a-b
                self.labelOperacion.setText(c)
            elif elecc==3:
                a=random.randrange(50)
                b=random.randrange(10)            
                c=str(a)+" x "+str(b)
                self.resultadoReal=a*b
                self.labelOperacion.setText(c)
            elif elecc==4:
                a=random.randrange(100)
                b=random.randrange(1,11)            
                c=str(a)+" / "+str(b)
                self.resultadoReal=int(a/b)
                self.labelOperacion.setText(c)
            
            opc.append(int(self.resultadoReal))
            opc.append(int(self.resultadoReal+random.randrange(-20,20)))
            opc.append(int(self.resultadoReal+random.randrange(-20,20)))
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado1.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado2.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado3.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)    
           

        elif self.parent().modoJuego==1: #Modo Sumas
            a=random.randrange(50)
            b=random.randrange(50)
            c=str(a)+"+"+str(b)
            self.resultadoReal=a+b
            self.labelOperacion.setText(c)
           
            opc.append(self.resultadoReal)
            opc.append(self.resultadoReal+random.randrange(-20,20))
            opc.append(self.resultadoReal+random.randrange(-20,20))
            #print(opc)
            eleccion=random.choice(opc)
            self.labelResultado1.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            #print(opc)
            eleccion=random.choice(opc)
            self.labelResultado2.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            #print(opc)
            eleccion=random.choice(opc)
            self.labelResultado3.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            #print(opc)        
        elif self.parent().modoJuego==2: #Modo Restas
            a=random.randrange(50)
            b=random.randrange(50)
            while b>a:
                b=random.randrange(50)
            c=str(a)+"-"+str(b)
            self.resultadoReal=a-b
            self.labelOperacion.setText(c)
           
            opc.append(self.resultadoReal)
            opc.append(self.resultadoReal+random.randrange(-20,20))
            opc.append(self.resultadoReal+random.randrange(-20,20))
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado1.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado2.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado3.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)        
        elif self.parent().modoJuego==3: #Modo Multiplicacion
            a=random.randrange(50)
            b=random.randrange(10)            
            c=str(a)+" x "+str(b)
            self.resultadoReal=a*b
            self.labelOperacion.setText(c)
           
            opc.append(self.resultadoReal)
            opc.append(self.resultadoReal+random.randrange(-20,20))
            opc.append(self.resultadoReal+random.randrange(-20,20))
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado1.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado2.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado3.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)        
        elif self.parent().modoJuego==4: #Modo Division
            a=random.randrange(100)
            b=random.randrange(1,11)            
            c=str(a)+" / "+str(b)
            self.resultadoReal=int(a/b)
            self.labelOperacion.setText(c)
           
            opc.append(int(self.resultadoReal))
            opc.append(int(self.resultadoReal+random.randrange(-20,20)))
            opc.append(int(self.resultadoReal+random.randrange(-20,20)))
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado1.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado2.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)
            eleccion=random.choice(opc)
            self.labelResultado3.setText(str(eleccion))
            opc.remove(eleccion)
            eleccion=0
            print(opc)        
       
    def showEvent(self,event):
        self.labelPerfil.setText(str(self.parent().jugador))
        self.labelVidas.setText("3")        
        self.labelPts.setText("0")
        self.juego()

