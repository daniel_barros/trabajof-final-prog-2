from PyQt5.QtWidgets import QMainWindow, QApplication,QDialog
from PyQt5 import uic
from PyQt5.QtCore import Qt
import sqlite3

class Puntaje(QMainWindow):
    def __init__(self,parent):
        super().__init__(parent=parent)
        uic.loadUi("ui/highScores.ui", self)
        self.conexion=sqlite3.connect('src/players.db')
        self.cursor=self.conexion.cursor()  
        self.botonAtras.clicked.connect(self.cerrarymostrar)
    
    def cerrarymostrar(self):
        self.parent().prueba="Hola"
        self.close()
        self.parent().show()
    def closeEvent(self, event):
        self.close()
        self.parent().show()

    def cargar(self):
        self.cursor.execute("select * from jugador")
        jugadores=list(self.cursor.fetchall())
        print(jugadores)
        jugadores.sort(key=lambda x: x[2],reverse=True)
        print(jugadores)
        print("Cantidad de jugadores cargados: "+str(len(jugadores)))

    def showEvent(self,event):
        self.cursor.execute("select * from jugador")
        jugadores=list(self.cursor.fetchall())
        jugadores.sort(key=lambda x: x[2],reverse=True)

        for i in range(len(jugadores)):
            if i==0:
                aux=str(jugadores[i][1])+" -> "+str(jugadores[i][2])+"Pts."
                self.label1st_text.setText(aux)
            if i==1:
                aux=str(jugadores[i][1])+" -> "+str(jugadores[i][2])+"Pts."
                self.label2nd_text.setText(aux)
            if i==2:
                aux=str(jugadores[i][1])+" -> "+str(jugadores[i][2])+"Pts."
                self.label3rd_text.setText(aux)
            if i==3:
                aux=str(jugadores[i][1])+" -> "+str(jugadores[i][2])+"Pts."
                self.label4th_text.setText(aux)
            if i==4:
                aux=str(jugadores[i][1])+" -> "+str(jugadores[i][2])+"Pts."
                self.label5th_text.setText(aux)